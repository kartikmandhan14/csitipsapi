const express = require("express");
const cors = require("cors");
const app = express();
const { tips } = require("./tips.json");
var fs = require("fs");

app.use(cors());
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app
  .route("/tips")
  .get((req, res) => {
    res.send(tips);
  })
  .post((req, res) => {
    fs.readFile("./tips.json", "utf-8", function (err, file) {
      if (err) {
        res.send("error occured in reading the file");
        throw err;
      }

      const arrayOfObjects = JSON.parse(file);
      const newTip = req.body;
      newTip.id = arrayOfObjects.tips.length + 1;
      arrayOfObjects.tips.push(newTip);
      // console.log(newTip);
      fs.writeFile(
        "./tips.json",
        JSON.stringify(arrayOfObjects),
        "utf-8",
        function (err) {
          if (err) {
            res.send("error occured in writing to file");
            throw err;
          }
          console.log("Done!");
        }
      );
    });
    res.sendStatus(200);
  });
app.get("/randomTip", (req, res) => {
  const r = Math.floor(Math.random() * tips.length);
  res.send(tips[r]);
});
app.get("/tipoftheday", (req, res) => {
  try {
    const date = new Date().getDate();
    const index = date % tips.length;
    res.send(tips[index]);
  } catch (error) {
    res.send(error);
  }
});

app.post("/shuffleTips", (req, res) => {
  fs.readFile("./tips.json", "utf-8", function (err, file) {
    if (err) throw err;
    const arrayOfObjects = JSON.parse(file);
    const oldTips = arrayOfObjects.tips;
    const newTips = oldTips.slice().sort(() => Math.random() - 0.5);
    // console.log(newTips);
    arrayOfObjects.tips = newTips;
    // console.log(JSON.stringify(arrayOfObjects));
    fs.writeFile(
      "./tips.json",
      JSON.stringify(arrayOfObjects),
      "utf-8",
      function (err) {
        if (err) throw err;
        console.log("Done!");
      }
    );
  });
  res.sendStatus(200);
});

//////////////////////////////////////////requests targetting a specific article///////////////////////////////
app.route("/tips/:id").get((req, res) => {
  //   console.log(req.params.id);
  const newTip = tips.filter((tip) => {
    return tip.id === parseInt(req.params.id);
  });
  res.send(newTip);
});

const PORT = 4000;
app.listen(PORT, function () {
  console.log(`server running on port ${PORT}`);
});
